#############################################################
# Random Forests
#############################################################
require(randomForest)
trainRFHard <- function(Expression, Y, ntree = 1000) {
  classes <- factor(max.col(Y))  
  Salida <- randomForest(Expression, classes, ntree = ntree)
  return(Salida)
}

predictRFHard <- function(Salida, Expression) {
  Treatment <- predict(Salida, Expression)
  return(Treatment)
}

VotesAssignment <- function(Proportion, Votes=10) {
  # Assign the votes according to a vector or probabilities
  Proportion <- Proportion/sum(Proportion) # Ensure that sum up to one
  VotesAssigned <- round(Votes*Proportion)
  while (sum(VotesAssigned) != Votes) {
    if (sum(VotesAssigned) > Votes) { # Too many votes
      Quitode <- which.max(VotesAssigned/Votes - Proportion)
      VotesAssigned[Quitode] <- VotesAssigned[Quitode] -1
    } else { #Too few votes
      Pongoen <- which.min(VotesAssigned/Votes - Proportion)
      VotesAssigned[Pongoen] <- VotesAssigned[Pongoen] +1
    }
  }
  return(VotesAssigned)
}

trainRFSoft2 <- function(Expression, Y, ntree = 10 * ncol(Y), NofVotes = 10) {
  # RF. Each drug is repeated according to the number of assigned votes
  
  BigExpression <- Expression[rep(1:nrow(Y), each = NofVotes),]
  dummy <- apply(Y,1,VotesAssignment,NofVotes)
  classes <- factor(rep(rep(1:ncol(Y), nrow(Y)), as.numeric(dummy)))
  Salida <- randomForest(BigExpression, classes, 
                         ntree = ntree, 
                         sampsize = ncol(Y))
}

# library(xgboost)

# Trial to apply xbboost to do RF. Too slow (hunderds of times slower!)

# trainRFSoft3 <- function(Expression, Y, ntree = 10 * ncol(Y), NofVotes = 10) {
#   # Create a big expression matrix with multiple copies of the original
#   big_expression <- Expression[rep(1:nrow(Y), each = NofVotes),]
#   
#   # Create a matrix with dummy labels for each vote
#   dummy_labels <- apply(Y,1,VotesAssignment,NofVotes)
#   
#   # Convert the dummy labels to a factor
#   classes <- factor(rep(rep(1:ncol(Y), nrow(Y)), as.numeric(dummy_labels)))
#   # xgb_params <- list("objective" = "multi:softprob",
#   #                    "eval_metric" = "mlogloss","sample_type"="weighted",
#   #                    "num_class" = ncol(Y), "tree_method" = "hist",
#   #                    "booster" = "gbtree")
#   # Train the random forest model using XGBoost
#   
#   rf_model <- xgboost(data = as.matrix(big_expression), 
#                       label = classes, "objective" = "multi:softprob",
#                       "eval_metric" = "mlogloss",
#                       nrounds = 1, num_parallel_tree = ntree, subsample = 0.5, 
#                       num_class = ncol(Y),"tree_method" = "hist")
#   
#   return(rf_model)
# }

trainRFSoft <- function(Expression, Y, ntree = 10 * ncol(Y)) {
  Votes <- Y

  # Build Big matrices
  # The expression matrix must be repeated by rows as many times as number of drugs. 
  # Each of them will have a single class with a corresponding weight given by the votes.
  
  BigExpression <- Expression[rep(1:nrow(Y), ncol(Y)),]
  classes <- factor(rep(1:ncol(Y), each = nrow(Y)))
  weights <- as.vector(t(Votes))
  
  Salida <- randomForest(BigExpression, classes, weights = weights, 
                         ntree = ntree, 
                         sampsize = ncol(Y))
  return(Salida)
}

predictRF <- function(Salida, Expression) {
  Treatment <- predict(Salida, Expression)
  return(Treatment)
}

predictRFSoft <- function(Salida, Expression) {
  Treatment <- predict(Salida, Expression, type = "prob")
  return(Treatment)
}

#############################################################
# XgBoost
#############################################################
require(xgboost)
trainXGB <- function(Expression, Y, nrounds = 50) {
  Votes <- Y
  
  #XGBoost giving different weights according to the pseudo-probs
  
  # Build Big matrices
  # The expression matrix must be repeated by rows as many times as number of drugs. 
  # Each of them will have a single class with a corresponding weight given by the votes.
  
  BigExpression <- Expression[rep(1:nrow(Y), each = ncol(Y)),]
  classes <- factor(rep(1:ncol(Y), nrow(Y)))
  weights <- as.vector(t(Votes))
  
  xgb_params <- list("objective" = "multi:softprob",
                     "eval_metric" = "mlogloss","sample_type"="weighted",
                     "num_class" = ncol(Y), "tree_method" = "gpu_hist",
                     "booster" = "gbtree")
  
  Salida <- xgboost(params = xgb_params, data = BigExpression, 
                                   label = as.numeric(classes)-1, weight = weights,
                                   max_depth = 3, eta = .3, nthread = 8, 
                                   nrounds = nrounds, subsample = 1,
                                   print_every_n = 1)
  return(Salida)
}

trainXGB2 <- function(Expression, Y, NofVotes = 4, max_depth = 3, nrounds = 50) {
  # XGBoost repeating the samples according to the voting system
  
  BigExpression <- Expression[rep(1:nrow(Y), each = NofVotes),]
  dummy <- apply(Y,1,VotesAssignment,NofVotes)
  classes <- factor(rep(rep(1:ncol(Y), nrow(Y)), as.numeric(dummy)), levels = 1:ncol(Y))

  xgb_params <- list("objective" = "multi:softprob",
                     "eval_metric" = "mlogloss",
                     "num_class" = ncol(Y), "tree_method" = "gpu_hist",
                     "booster" = "gbtree")
  Salida <- xgboost(params = xgb_params, data = BigExpression, 
                    label = as.numeric(classes)-1,
                    max_depth = max_depth, eta = .3, nthread = 8, 
                    nrounds = nrounds, subsample = 1, verbose =1,
                    print_every_n = 10L)
}




predictXGB <- function(Salida, Expression) {
  Treatment <- matrix(predict(Salida, Expression), ncol = Salida$params$num_class, byrow = T)
  Treatment <- max.col(Treatment)
  return(Treatment)
}

predictXGBSoft <- function(Salida, Expression) {
  Treatment <- matrix(predict(Salida, Expression), ncol = Salida$params$num_class, byrow = T)
  return(Treatment)
}
