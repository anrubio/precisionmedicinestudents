# Script
library(readxl)
library(RColorBrewer)
library(matrixStats)
library(partykit)
library(glmnet)
library(BOSO)


source("Code_Analysis/XAIfunctions_NV.R")

library(impute)
library(knitr)
library(reticulate)
library(readxl)
library(readr)
library(limma)
library(ggplot2)
library(ggpubr)
library(ggsci)
library(matrixStats)
library(maftools)
library(qvalue)
library(tibble)
library(Rtsne)
library(IHW)
library(matrixStats)
library(pheatmap)
library(DT)
library("dplyr")
library(tictoc)

source("./MOM/2022-05-24_MOM_source_Functions.R")
source("./MOM/predictMOM_ALL.R")
source("Code_Analysis/XAIfunctions_XGBoost.R")
load("C:/Users/arubio/OneDrive - Tecnun/GitRepositories/precisionmedicinestudents/prueba.RData")

# Keep genes with the largest variance
ngenes <- 1000
Expression <- OldExpression
sdevs <- rowSds(OldExpression)

Keep <- which(order(sdevs, decreasing = T) <= ngenes)
Expression <- Expression[Keep,]

commonPatients <- intersect(rownames(C), colnames(Expression))
Expression <- Expression[,commonPatients]
Expression <- t(Expression)

C <- C[commonPatients, ]

X<-mut2
X<-X[commonPatients, ]

Y <- exp(-2*C) # Convert weights into pseudoprobabilities
Y <- Y /rowSums(Y) # Probabilities sum up one

# Variables:
# X: mutations 257 patients x 69 mutations
# Expression: Gene expression 257 patients x 1000 genes
# C: response to drugs: 257 patients x 111 drugs (larger values --> less effective)
# Y: Conversion from C to "vote probabilities" same size as C (larger values --> more effective)


###############################################
# K-fold cross-validation
###############################################

set.seed(2022)
Folds <- 5
Groups <- sample(1:Folds, nrow(C), replace = T)


# ODT Mutation
TratamientoTreeMut <- rep(NA, length(Groups))
cat("ODT mutation:\n")


for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  treeMut <- trainTreeMut(X[Train,], C[Train,], minbucket = 10)
  TratamientoTreeMut[Test] <- predictTreeMut(treeMut,C[Test,], X[Test,])
}

# ODT Expression
TratamientoTreeExp <- rep(NA, length(Groups))
cat("ODT Expression:\n")


for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  treeExp <- trainTree(Expression[Train,], C[Train,], minbucket = 10)
  TratamientoTreeExp[Test] <- predictTree(treeExp,C[Test,], Expression[Test,])
}

# RandomForest Hard (Mutations)
TratamientoRFHMut <- rep(NA, length(Groups))
cat("RF Hard mutations:\n")

for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaRFHMut <- trainRFHard(X[Train,], Y[Train,], ntree = 1000)
  TratamientoRFHMut[Test] <- predictRF(SalidaRFHMut,X[Test,])
}


# RandomForest Hard (Expression)
TratamientoRFHExp <- rep(NA, length(Groups))
cat("RF Hard expression:\n")


for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaRFHExp <- trainRFHard(Expression[Train,], Y[Train,], ntree = 1000)
  TratamientoRFHExp[Test] <- predictRF(SalidaRFHExp,Expression[Test,])
}

# RandomForest voting system (Expression)
TratamientoRFS2Exp <- rep(NA, length(Groups))
cat("RF Voting Expression:\n")

for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaRFS2Exp <- trainRFSoft2(Expression[Train,], Y[Train,], ntree = 1000, NofVotes = 3)
  TratamientoRFS2Exp[Test] <- predictRF(SalidaRFS2Exp,Expression[Test,])
}

# RandomForest voting system (Mutations)
TratamientoRFS2Mut <- rep(NA, length(Groups))

cat("RF Voting mutations:\n")

for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaRFS2Mut <- trainRFSoft2(X[Train,], Y[Train,], ntree = 1000, NofVotes = 3)
  TratamientoRFS2Mut[Test] <- predictRF(SalidaRFS2Mut,X[Test,])
}
source("Code_Analysis/XAIfunctions_XGBoost.R")

# XGBoost voting system (Expression)
TratamientoXGB2Exp <- rep(NA, length(Groups))
cat("XGBoost Voting Expression:\n")

for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaXGB2Exp <- trainXGB2(Expression[Train,], Y[Train,], NofVotes = 3)
  TratamientoXGB2Exp[Test] <- predictXGB(SalidaXGB2Exp,Expression[Test,])
}

# XGBoost voting system (Mutations)
TratamientoXGB2Mut <- rep(NA, length(Groups))
cat("XGBoost Voting Mutation:\n")

for (fold in 1:Folds) {
  cat("fold: ", fold, "\n")
  Test <- Groups == fold
  Train <- !Test
  SalidaXGB2Mut <- trainXGB2(X[Train,], Y[Train,], NofVotes = 3)
  TratamientoXGB2Mut[Test] <- predictXGB(SalidaXGB2Mut,X[Test,])
}

# TODO: include other types of RF, mutations, etc.
# Use tic() toc() from the tictoc library to measure computation times...
# Check the sprint package to do randomforest in parallel.

TratamientoOracle <- apply(C, 1, which.min)
Tratamiento_plot <- data.frame(IC50 = C[cbind(1:nrow(C), TratamientoOracle)], Method = "0. Oracle")
Tratamiento_plot <- rbind(Tratamiento_plot,
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoTreeMut)], Method = "1b. ODT (Mut)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoTreeExp)], Method = "1a. ODT (Exp)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoRFHExp)], Method = "2a. Random Forest Hard (Exp)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoRFHMut)], Method = "2b. Random Forest Hard (Mut)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoRFS2Exp)], Method = "3a. Random Forest Voting (Exp)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoRFS2Mut)], Method = "3b. Random Forest Voting (Mut)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoXGB2Mut)], Method = "4b. XGBoost Voting (Mut)"),
                          data.frame(IC50 = C[cbind(1:nrow(C), TratamientoXGB2Exp)], Method = "4a. XGBoost Voting (Exp)"))
ggplot(Tratamiento_plot, aes(x=Method, y=IC50, fill=Method))+geom_boxplot()+
  theme_bw()+scale_fill_npg()+ylab("IC50*")+ggtitle("BeatAML 5-fold Cross Validation")+
  theme(axis.text.x = element_text(angle = 15, hjust = 1))

