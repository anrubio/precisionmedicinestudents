library(readxl)
library(party)
library(data.tree)
library(matrixStats)
library(partykit)
library(ggparty)
library(pheatmap)
library(randomForest)

setwd("C:/Users/arubio/OneDrive - Tecnun/GitRepositories/aml_patients_ic50")
source("./code_AR/TreeOptimizers.R")

load("./code_AR/Drug&mut.RData")
Drug_Names_BeatAML <- read_excel("./code_AR/Drug_Names_BeatAML.xlsx")

Include <- Drug_Names_BeatAML$`Drug Names BeatAML`[grep("AML-", Drug_Names_BeatAML$Code)]
# Two drugs are missing
Include[which(is.na(match(Include, rownames(drug))))]
Include <- Include[which(!is.na(match(Include, rownames(drug))))]
drug <- drug[Include,]

# Center the IC50 matrix (remove mean value of IC50)
drugDetrend <- as.matrix(drug) - rowMeans(as.matrix(drug))

# Compute the cost matrix
C <- t(drugDetrend)
C <- C - rowMins(C)

X <- mut
Keep <- colMeans(X) > .02 # Mutation must appear in 2% of the samples
X <- X[,Keep]

mode(X) <- "integer"
# Read expression data
Expression <- read_excel("./data/input/41586_2018_623_MOESM3_ESM.xlsx",
                         sheet = "Table S8-Gene Counts RPKM")

# Keep genes with the largest variance
ngenes <- 10000
Expression <- as.data.frame(Expression)
rownames(Expression) <- Expression[,2]
Expression <- Expression[,-c(1,2)]
OldExpression <- as.matrix(Expression)
sdevs <- rowSds(OldExpression)
Keep <- which(order(sdevs, decreasing = T) <= ngenes)
Expression <- Expression[Keep,]

commonPatients <- intersect(colnames(drug), colnames(Expression))
Expression <- Expression[,commonPatients]
Expression <- t(Expression)

drug <- drug[, commonPatients]
drugDetrend <- as.matrix(drug) - rowMeans(as.matrix(drug))

# Compute the cost matrix
C <- t(drugDetrend)
# C <- t(medpolish(drugDetrend)$residuals) # Another possibility.

C <- C - rowMins(C)
pheatmap(t(C), show_colnames=F)


# Lower is better -more effective

# Votes for each drug
gamma <- 10
Votes <- exp(-C*gamma)
Votes <- Votes / rowSums(Votes)

# Build Big matrices
# The expression matrix must be repeated by rows as many times as number of drugs. 
# Each of them will have a single class with a corresponding weight given by the votes.

BigExpression <- Expression[rep(1:nrow(C), ncol(C)),]
BigMut <- X[rep(1:nrow(C), ncol(C)),]

# m <- ncol(C)  # set the size of the identity matrix
# n <- nrow(C)  # set the number of times to repeat the matrix
# 
# identity_matrix <- diag(m)  # create the identity matrix
# classes <- do.call(rbind, replicate(n, identity_matrix, simplify = FALSE))  # concatenate the matrices

classes <- factor(rep(1:ncol(C), nrow(C)))
weights <- as.vector(t(Votes))

Salida <- randomForest(BigMut, classes, weights = weights, 
                       do.trace = 100, 
                       ntree = 1000 * ncol(C) , 
                       sampsize = ncol(C))
plot(Salida)

# Looks awful. Nevertheless, the interface does not deal well with the weights.
preds <- predict(Salida, type = "prob")
n = 0
n = n+1
plot(preds[n,], pch = 16, col = "#00000055", ylim = c(0,1))
points(Votes[n,], col = "#FF000055", pch = 16)
cor(preds[n,], Votes[n,])


# Let's see Xgboost

library(xgboost)
xgb_params <- list("objective" = "multi:softprob",
                   "eval_metric" = "mlogloss",
                   "num_class" = ncol(C), "tree_method" = "gpu_hist")

nround    <- 100 # number of XGBoost rounds
cv.nfold  <- 5

system.time(SalidaXGB <- xgboost(params = xgb_params, data = BigExpression, 
                     label = as.numeric(classes)-1, weight = weights,
                     max_depth = 2, eta = .2, nthread = 8, 
                     nrounds = nround, subsample = 8/ncol(C),
                     print_every_n = 1L, lambda = 10, alpha = 0))

pred <- predict(SalidaXGB, BigExpression)
pred <- matrix(pred, ncol=ncol(C), byrow=TRUE)


importance_matrix = xgb.importance(feature_names = colnames(BigExpression), model = SalidaXGB)
xgb.ggplot.importance(importance_matrix[1:20,])
xgb.ggplot.importance(importance_matrix)

Keep <- importance_matrix$Feature[1:500]
Expression2 <- Expression[,Keep]
BigExpression <- Expression2[rep(1:nrow(C), ncol(C)),]


nround    <- 500 # number of XGBoost rounds
cv.nfold  <- 5

SalidaXGB <- xgboost(params = xgb_params, data = BigExpression, 
                     label = as.numeric(classes)-1, weight = weights,
                     max_depth = 5, eta = .1, nthread = 8, 
                     nrounds = nround, subsample = 8/ncol(C),
                     print_every_n = 5L, lambda = .5, alpha = .1)

n = 0
n = n+1
pred <- predict(SalidaXGB, Expression2[n,,drop=F])
plot(pred, pch = 16, col = "#00000055", ylim = c(0,1))
points(Votes[n,], col = "#FF000055", pch = 16)
cor(pred, Votes[n,], method = "spearman")

